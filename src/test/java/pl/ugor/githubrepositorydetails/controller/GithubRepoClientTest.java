package pl.ugor.githubrepositorydetails.controller;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class GithubRepoClientTest {

    String existingUser = "ugor1984";
    String repositoryWithDescription= "GitHub-Api-test";
    String repositoryWithoutDescription = "GitHub-Api-test-without-description";
    String nonExistingValue = String.valueOf(UUID.randomUUID());
    UUID nonExistingUser = UUID.randomUUID();
    UUID nonExistingRepository = UUID.randomUUID();

    @LocalServerPort
    int randomServerPort;

    @Test
    public void shouldReturnRepositoryDetails() {

        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<Object> result = restTemplate.getForEntity("http://localhost:" + randomServerPort + "/repositories/" + existingUser + "/" + repositoryWithDescription, Object.class);
        String body = result.getBody().toString().substring(1, result.getBody().toString().length() - 1).replaceAll("\\s", "");
        String[] bodyArray = body.split(",");
        String fullName = bodyArray[0];
        String description = bodyArray[1];
        String cloneUrl = bodyArray[2];
        String stars = bodyArray[3];
        String createdAt = bodyArray[4];

        Assert.assertEquals(200, result.getStatusCodeValue());
        Assert.assertTrue(fullName.matches("fullName=.+/.+"));
        Assert.assertTrue(description.matches("description=.*"));
        Assert.assertTrue(cloneUrl.matches("cloneUrl=https://github.com/.+git"));
        Assert.assertTrue(stars.matches("stars=\\d*"));
        Assert.assertTrue(createdAt.matches("createdAt=\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}Z"));
        Assert.assertFalse(result.getBody().toString().contains(nonExistingValue));
        System.out.println(nonExistingValue);
    }

    @Test
    public void shouldReturnRepositoryWithoutDescriptionDetails() {

        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<Object> result = restTemplate.getForEntity("http://localhost:" + randomServerPort + "/repositories/" + existingUser + "/"+ repositoryWithoutDescription, Object.class);

        Assert.assertEquals(200, result.getStatusCodeValue());
        Assert.assertTrue(result.getBody().toString().matches("^\\{fullName=.+/.+,\\sdescription=.*,\\scloneUrl=https://github.com/.+git,\\sstars=\\d*,\\screatedAt=\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}Z}$"));
        Assert.assertFalse(result.getBody().toString().contains(nonExistingValue));
    }

    @Test
    public void shouldReturnNotFoundMessage() {

        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<Object> result = restTemplate.getForEntity("http://localhost:" + randomServerPort + "/repositories/"+nonExistingUser+"/"+nonExistingRepository, Object.class);

        Assert.assertEquals(404, result.getBody());
    }

}
