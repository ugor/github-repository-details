package pl.ugor.githubrepositorydetails;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GithubRepositoryDetailsApplication {

	public static void main(String[] args) {
		SpringApplication.run(GithubRepositoryDetailsApplication.class, args);
	}
/*
  v8 - Added regex matchers to the tests
  v7 - Fixed native variable's names issue and added packages
  v6 - Added to the tests assert false with wrong value
  v5 - Added possibility supply credencial at the endpoint parameters for reach request limit
  v4 - Removed exclusion from pom.xml for run all test from command line
  v3 - Added tests
  v2 - Added protection before wrong repo or user name supply
  v1 - Added main functional
  */
}
