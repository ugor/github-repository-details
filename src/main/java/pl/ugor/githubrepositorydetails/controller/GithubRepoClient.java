package pl.ugor.githubrepositorydetails.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import pl.ugor.githubrepositorydetails.model.GithubUserRepository;

@RestController
public class GithubRepoClient {

    @GetMapping("/repositories/{owner}/{repositoryName}")
    public Object getRepoByNameAndOwner(@PathVariable String owner, @PathVariable String repositoryName, @RequestParam(value = "client_id", required = false) String clientId, @RequestParam(value = "client_secret", required = false) String clientSecret) {

        final String usersRepositoryUrl = "https://api.github.com/repos/" + owner + "/" + repositoryName + "?client_id=" + clientId + "&client_secret=" + clientSecret;

        RestTemplate restTemplate = new RestTemplate();

        try {
            return restTemplate.getForObject(usersRepositoryUrl, GithubUserRepository.class);
        } catch (HttpClientErrorException | HttpServerErrorException httpClientOrServerExc) {
            return httpClientOrServerExc.getMessage();
        }
    }

    @GetMapping("/rate-limit")
    public String getRateLimit(@RequestParam(value = "client_id", required = false) String clientId, @RequestParam(value = "client_secret", required = false) String clientSecret) {

        final String rateLimitUrl = "https://api.github.com/rate_limit" + "?client_id=" + clientId + "&client_secret=" + clientSecret;

        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject(rateLimitUrl, String.class);
    }
}
